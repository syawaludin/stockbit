<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>edit_public_profile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>279d4092-321b-415f-a06b-6ed8c3a0a6b8</testSuiteGuid>
   <testCaseLink>
      <guid>889066c0-d21b-44f1-b6d3-99d70a191008</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc22c14d-92ee-45dd-80a0-9fd7a50d0f99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/public_profile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ab82afe-13d6-4c9b-a129-932957f9c4e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
