import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('home/menu_avatar/avatarr'))

WebUI.click(findTestObject('home/setting/public_profile/menu_settings'))

WebUI.click(findTestObject('home/setting/public_profile/menu_public_profile'))

WebUI.click(findTestObject('home/setting/public_profile/btn_edit_public_profile'))

WebUI.setText(findTestObject('home/setting/public_profile/textarea_Biography'), 'Saya adalah Syawal')

WebUI.selectOptionByValue(findTestObject('home/setting/public_profile/select_location'), 'DKI Jakarta', false)

WebUI.selectOptionByValue(findTestObject('home/setting/public_profile/select_gender'), 'male', false)

WebUI.selectOptionByValue(findTestObject('home/setting/public_profile/select_Birthday_Day'), '9', false)

WebUI.selectOptionByValue(findTestObject('home/setting/public_profile/select_Birthday_Month'), '4', false)

WebUI.selectOptionByValue(findTestObject('home/setting/public_profile/select_Birthday_Year'), '1992', false)

WebUI.setText(findTestObject('home/setting/public_profile/input_Website'), 'https://www.linkedin.com/in/syawaludin-efendi-sitanggang-12bb95b2/')

WebUI.setText(findTestObject('home/setting/public_profile/textarea_Address'), 'Perumahan SAI Residence')

WebUI.click(findTestObject('home/setting/public_profile/btn_Save'))

