import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('home/menu_avatar/avatarr'))

WebUI.click(findTestObject('home/setting/public_profile/menu_settings'))

WebUI.click(findTestObject('home/setting/Notification/menu_Notification'))

WebUI.click(findTestObject('home/setting/Notification/h3_Email'))

WebUI.click(findTestObject('home/setting/Notification/email_mentions'))

WebUI.click(findTestObject('home/setting/Notification/h3_Mobile'))

WebUI.click(findTestObject('home/setting/Notification/mobile_mentions'))

WebUI.click(findTestObject('home/setting/Notification/mobile_likes'))

WebUI.click(findTestObject('home/setting/Notification/mobile_replies'))

WebUI.click(findTestObject('home/setting/Notification/mobile_followed'))

WebUI.click(findTestObject('home/setting/Notification/mobile_reposts'))

WebUI.click(findTestObject('home/setting/Notification/mobile_followed_users_post'))

WebUI.click(findTestObject('home/setting/Notification/mobile_friend_on_Stockbit'))

WebUI.click(findTestObject('home/setting/Notification/mobile_Someone_messaged_me'))

WebUI.click(findTestObject('home/setting/Notification/mobile_message_request'))

WebUI.click(findTestObject('home/setting/Notification/mobile_company_news'))

WebUI.click(findTestObject('home/setting/Notification/mobile_tipping_received'))

WebUI.click(findTestObject('home/setting/Notification/mobile_tipping_sent'))

WebUI.click(findTestObject('home/setting/Notification/mobile_tipping_claimed'))

WebUI.click(findTestObject('home/setting/Notification/mobile_price_alert'))

WebUI.click(findTestObject('home/setting/Notification/mobile_trending_stocks'))

